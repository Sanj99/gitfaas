from xpms_storage.db_handler import DBProvider
import json
import time
import requests
from xpms_storage.utils import get_env


def hma_retrain_completed_notification(**obj):
    NAMESPACE = get_env("NAMESPACE", "claims-audit", False)
    DOMAIN_NAME = get_env("DOMAIN_NAME", "enterprise.xpms.ai", False)
    ENV_DATABASE = get_env('DATABASE_PARAPHRASE', None, True)
    BE_URL = get_env('CLAIMS_AUDIT_APIS_URL', None, True)

    notification = {
        "group": "retrain_status",
        "message": {
            "body": "Retrain pipeline completed.",
            "status": "success",
            "title": 'Retrain pipeline',
            "icon": "completed"
        },
        'metadata': {
            "name": 'Retrain Pipeline',
            'current_status': "completed"
        },
        "created_timestamp": int(time.time())
    }
    db = DBProvider.get_instance(db_name=ENV_DATABASE)
    s = db.insert(table='notifications', rows=[notification])

    if s:
        url = f'https://{BE_URL}/send_notification'
        headers = {
            'Content-Type': 'application/json'
        }

        requests.request("POST", url, headers=headers, data=json.dumps(notification, default=str))

    return {"status": "completed"}
